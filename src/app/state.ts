import {Activity, Attribute, Datatype, DateClass, Exercise, Service} from "./model";

export class State {
  currentService: Service | null;

  constructor(public services: Service[], public datatypes: Datatype[]) {
  }
}

export const datatypes = [
  new Datatype("Datum", []),
  new Datatype("Číslo", []),
  new Datatype("Text", []),
  new Datatype("Doba trvání", []),
  new Datatype("Den v týdnu", ["pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota", "neděle"]),
  new Datatype("Pohlaví", ["M", "Ž"])
  ];

  export const datatypesEmpty = [
  new Datatype("Datum", []),
  new Datatype("Číslo", []),
  new Datatype("Text", []),
  ];


export const emptyState = new State([], datatypesEmpty);

export const activity1 = new Activity('Aktivita 1', "Popis aktivity 1, ktery je vemlmi dlouhy aby to aspon vypadalo, ze tady neco je.");
export const fullStateNotReal = new State([
  new Service('Sluzba 1', 'Popis tehle sluzby', [
    activity1
      .addAttribute(new Attribute("Datum vykonání", datatypes[0]))
      .addAttribute(new Attribute("Počet najetých kilometrů", datatypes[1]))
      .addAttribute(new Attribute("Poznámka", datatypes[2]))
      .addExercise(new Exercise(activity1.getAttributesNames(), [new DateClass(2021, 3, 20), "200", "Zbytecna poznamka"]))
      .addExercise(new Exercise(activity1.getAttributesNames(), [new DateClass(2018, 7, 20), "10", "Poznamka"])),
    new Activity('Aktivita se strasne dlouhym nazvem a tak', 'Description'),
  ]),
  new Service('Prazdna sluzba', 'Tahle sluzba nema zadne aktivity', []),
  new Service('Sluzba 1', 'Popis tehle sluzby', [
    new Activity('Aktivita se strasne dlouhym nazvem a tak', 'Description'),
    new Activity('Aktivita 1', 'Description'),
    new Activity('Aktivita se strasne dlouhym nazvem a tahle s jeste delsim :)', 'Description'),
    new Activity('Aktivita 2', 'Description'),
    new Activity('Aktivita 2', 'Description'),
  ]),
  new Service('Posledni sluzba, tahle ma taky strasne dlouhy nazev, doufam, ze se vsude vejde. Rozhodne by bylo fine, kdyby jo!', 'Popis tehle sluzby: Posledni sluzba, tahle ma taky strasne dlouhy nazev, doufam, ze se vsude vejde. Rozhodne by bylo fine, kdyby jo! Posledni sluzba, tahle ma taky strasne dlouhy nazev, doufam, ze se vsude vejde. Rozhodne by bylo fine, kdyby jo!', [
    new Activity('Aktivita se strasne dlouhym nazvem a tak', 'Description'),
    new Activity('Aktivita 1', 'Description'),
    new Activity('Tohle uz je ale extrem... Aktivita se strasne dlouhym nazvem a tahle s jeste delsim :) Aktivita se strasne dlouhym nazvem a tahle s jeste delsim :)', 'Description'),
    new Activity('Aktivita 2', 'Description'),
    new Activity('Aktivita 2', 'Description'),
  ]),
],
  datatypes
);

// real data
// sluzba Poradna
export const servicePoradnaActivityPravni = new Activity("Právní poradenství", "Neznalost práva může přivodit vážné problémy. Občas je tedy nutné vyhledat pomoc právního specialisty. Pro lidi v nouzi je k dispozici právní poradenství zdarma, které poskytuje JUDr. Dominik Refi.")
  .addAttribute(new Attribute("Popis problému", datatypes[2]))
  .addAttribute(new Attribute("Řešení problému", datatypes[2]))
export const servicePoradnaActvityFinancni = new Activity("Finanční poradenství", "Individuální finanční a dluhové poradenství pro seniory. Finanční specialistka Ing. Eva Krhovská vám poradí v oblasti financí, rodinného rozpočtu, půjček, pojištění a sociálních dávek. Pokud jste například podepsali nevýhodnou smlouvu, nebo se změnila vaše životní situace a nedokážete splácet, či váháte, zda je lepší investovat, nebo se nechat pojistit, obraťte se na naši finanční poradnu.")
  .addAttribute(new Attribute("Popis problému", datatypes[2]))
  .addAttribute(new Attribute("Řešení problému", datatypes[2]))

servicePoradnaActvityFinancni.addExercise(new Exercise(servicePoradnaActvityFinancni.getAttributesNames(), ["Eva Krhovská","František Bezpeněz",new DateClass(2021, 3, 20),"Pan Bezpeněz přišel o veškerý majetek kvůli závislosti na výherních automatech. Nyní je v exekuci a potřebuje právní pomoc.","S klientem byly probrány možnosti oddlužení a zároveň byl odkázán na lokální ordinaci adiktologa."]
))
// sluzba Nocelaharna
export const serviceNocleharnaActivityNocleh = new Activity("Nocleh", "Poskytnutí jednorázové pomoci (noclehu) osobám bez přístřeší.")
  .addAttribute(new Attribute("Cena", datatypes[1]))
  .addAttribute(new Attribute("Pohlaví", datatypes[5]))
  .addAttribute(new Attribute("Poznámka", datatypes[2]))
serviceNocleharnaActivityNocleh
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Dobroslav Kubík", new DateClass(2021, 11, 21), "15 Kč", "M", "Prvni vykon"]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Bernard Kovář", new DateClass(2021, 11, 21), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Drahoslav Kraus", new DateClass(2021, 11, 1), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Otakar Sládek", new DateClass(2021, 3, 21), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Jarmil Ztratil", new DateClass(2021, 11, 21), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Igor Řehák", new DateClass(2021, 11, 21), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Vlastimil Hanák", new DateClass(2021, 5, 21), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Anežka Janková", new DateClass(2021, 11, 21), "15 Kč", "Ž", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Ingrid Kubíková", new DateClass(2021, 1, 1), "15 Kč", "Ž", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Milan Suchánek", new DateClass(2021, 11, 21), "15 Kč", "M", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Sabina Bendová", new DateClass(2021, 11, 21), "15 Kč", "Ž", ""]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Adolf Vlček", new DateClass(2021, 12, 1), "50 Kč", "M", "Mrznul" ]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Květa Franková", new DateClass(2021, 11, 1), "30 Kč", "Ž", "" ]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Adolf Vlček", new DateClass(2021, 1, 21), "30 Kč", "M", "" ]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Josef Novák", new DateClass(2021, 11, 21), "30 Kč", "M", "" ]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Kamil Hanuš", "Miriam Polášková", new DateClass(2021, 1, 21), "30 Kč", "Ž", "" ]))
  .addExercise(new Exercise(serviceNocleharnaActivityNocleh.getAttributesNames(), ["Agáta Málková", "Denisa Pykalová", new DateClass(2021, 1, 5), "30 Kč", "Ž", "" ]))
export const serviceNocleharnaActivityPradlo = new Activity("Praní a sušení prádla", "Služba praní a sušení prádla.")
  .addAttribute(new Attribute("Cena", datatypes[1]))
serviceNocleharnaActivityPradlo
  .addExercise(new Exercise(serviceNocleharnaActivityPradlo.getAttributesNames(), ["Kamil Hanuš", "Jan Špinavý", new DateClass(2021, 1, 12), "30"]))
  .addExercise(new Exercise(serviceNocleharnaActivityPradlo.getAttributesNames(), ["Kamil Hanuš", "Karel Mokrý", new DateClass(2021, 12, 22), "30"]))
  .addExercise(new Exercise(serviceNocleharnaActivityPradlo.getAttributesNames(), ["Kamil Hanuš", "Jan Špinavý", new DateClass(2021, 6, 21), "30"]))

// sluzba Vydejna
export const serviceVydejnaActivityVydejJidla = new Activity("Výdej jídla", "Poskytnutí dočasné potravinové, hygienické nebo materiální pomoci osobám v hmotné nouzi.")
  .addAttribute(new Attribute("Popis vydaného jídla", datatypes[2]))
serviceVydejnaActivityVydejJidla
  .addExercise(new Exercise(serviceVydejnaActivityVydejJidla.getAttributesNames(), ["Marina Nosková", "Evžen Adámek", new DateClass(2021, 1, 4), "3 chleby"]))
  .addExercise(new Exercise(serviceVydejnaActivityVydejJidla.getAttributesNames(), ["Marina Nosková", "Dáša Přibylová", new DateClass(2021, 11, 12), "Mouka a trvanlivé mléko"]))
  .addExercise(new Exercise(serviceVydejnaActivityVydejJidla.getAttributesNames(), ["Aleš Kubíček", "Šárka Marečková", new DateClass(2021, 10, 12), "Zelenina, chleba"]))
  .addExercise(new Exercise(serviceVydejnaActivityVydejJidla.getAttributesNames(), ["Aleš Kubíček", "Alena Kašpárková", new DateClass(2021, 1, 2), "10 jogurtů"]))
  .addExercise(new Exercise(serviceVydejnaActivityVydejJidla.getAttributesNames(), ["Marina Nosková", "Aneta Kočová", new DateClass(2021, 1, 6), "Mouka a trvanlivé mléko"]))
export const serviceVydejnaActivityVydejMaterialu = new Activity("Výdej materiální pomoci", "V průběhu roku mohou klienti využít darů oblečení a bot (pro děti i dospělé), ale také věcí jako je: povlečení, prostěradla, ručníky, nádobí, hračky, kočárky, dětské sedačky, spacáky, karimatky, trvanlivé potraviny, drogerie, knihy aj. Občasně je možné zjistit materiální pomoc formou darování větších věcí jako jsou postele, skříně, lednice apod.")
  .addAttribute(new Attribute("Popis vydaného produktu", datatypes[2]))

export const fullStateReal = new State([
  new Service("Poradna pro lidi v nouzi", "Poradna pro lidi v nouzi je bezplatná služba je určena osobám, které potřebují pomoc a podporu při řešení své tíživé životní situace, kterou nejsou schopny vyřešit vlastními silami. Posláním Poradny pro lidi v nouzi je poskytnout podporu a pomoc osobě v tíživé životní situaci, kterou není schopna vyřešit vlastními silami.",[
    servicePoradnaActivityPravni,
    servicePoradnaActvityFinancni
  ]),
  new Service("Sociálně aktivizační služba pro rodiny s dětmi sv. Rity", "Provázíme rodiny s dětmi na cestě životem tak, aby získaly potřebné informace k řešení jejich těžkostí, získaly potřebnou míru podpory a sebejistoty, aby postupem času již k řešení těchto situací nepotřebovaly podporu pomáhajících institucí a byly schopny je řešit svépomocí.", [

  ]),
  new Service("Noclehárna sv. Antonína", "Posláním služby je jednorázově poskytnout nocleh a jednoduché zázemí osobám, které v důsledku nepříznivé životní situace nemají jinou alternativu přenocování nežli setrvat venku. Poskytováním noclehu, čistého prostředí a možnosti udržovat osobní hygienu pomáháme klientovi zvýšit kvalitu jeho života, popřípadě se snáze začlenit do společnosti.", [
    serviceNocleharnaActivityNocleh,
    serviceNocleharnaActivityPradlo
  ]),
  new Service("Nízkoprahové zařízení pro děti a mládež Kompas", "Nízkoprahové zařízení pro děti a mládež nabízí bezpečný prostor, podporu a pomoc dětem nebo mládeži k řešení jejich osobních problémů, bezpečnému trávení volného času. Chceme motivovat a pomoci klientům, aby našli sílu u sebe sama k řešení svých problémů nebo těžkostí.", [

  ]),
  new Service("Výdejna Samaritán", "Prvotní oblast, pomocí které může „Samaritán“ pomáhat osobám v nouzi, je díky dárcovství. V průběhu roku mohou klienti využít darů oblečení a bot (pro děti i dospělé), ale také věcí jako je: povlečení, prostěradla, ručníky, nádobí, hračky, kočárky, dětské sedačky, spacáky, karimatky, trvanlivé potraviny, drogerie, knihy aj. Občasně je možné zjistit materiální pomoc formou darování větších věcí jako jsou postele, skříně, lednice apod. (Nutné se předem domluvit se sociálními pracovníky s ohledem na skladovací prostory Výdejny Samaritán).", [
    serviceVydejnaActivityVydejJidla,
    serviceVydejnaActivityVydejMaterialu
  ])
], datatypes)

export const state = fullStateReal;
