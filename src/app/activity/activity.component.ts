
import {Activity, Attribute, DateClass, Exercise, PeriodicElement, Service} from "../model";
import {state} from "../state";
import {DialogEditActivity} from '../dialogs/dialog-basic-create-edit/dialog-edit-activity';
import {MatDialog} from '@angular/material/dialog';
import {DialogExerciseEdit} from '../dialogs/dialog-exercise-edit-detail/dialog-exercise-edit';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatSnackBar, MatSnackBarRef, TextOnlySnackBar} from "@angular/material/snack-bar";
import {DialogExerciseAdd} from '../dialogs/dialog-exercise-edit-detail/dialog-exercise-add';
import {DialogExerciseDetail} from '../dialogs/dialog-exercise-edit-detail/dialog-exercise-detail';
import {DialogActivityAddAttribute} from '../dialogs/dialog-activity-add-attribute/dialog-activity-add-attribute';
import { elementAt } from 'rxjs/operators';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})

export class ActivityComponent implements OnInit, AfterViewInit, OnDestroy {
  serviceId: number;
  activityId: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  actionColumns: string[] = ['actions']
  displayedColumns: string[] = []//= ['name', 'weight', 'symbol', 'position'];
  columnsToDisplay: string[] = []//= this.displayedColumns.slice();
  data: PeriodicElement[] = [] //ELEMENT_DATA;
  dataSource = new MatTableDataSource<PeriodicElement>()
  dateTypesNames : string[] = [];
  private openedSnackBar: MatSnackBarRef<TextOnlySnackBar>;

  private exercisesToBeDeleted: Exercise[] = [];
  private indexOfExerciseToBeDeleted: number[] = [];
  private rowsToBeDeleted: any[] = [];
  private isTimeout:boolean = true;

  constructor(private router: Router, private changeDetectorRefs: ChangeDetectorRef, public dialog: MatDialog,
    private route: ActivatedRoute, private _snackBar: MatSnackBar) {
    this.route.params.subscribe(params => {
      this.serviceId = params['serviceId'];
      this.activityId = params['activityId'];
    })
  }

  ngOnInit(): void {
    // add column names dynamically
    this.getActivity(this.serviceId, this.activityId).attributes.forEach(attribute => {
      this.columnsToDisplay.push(attribute.name)
      this.displayedColumns.push(attribute.name)
      if(attribute.datatype.name === "Datum")
        this.dateTypesNames.push(attribute.name);
    })
    this.displayedColumns.push(...this.actionColumns)

    this.getActivity(this.serviceId, this.activityId).exercises.forEach(exercise => {
      this.data.push(exercise.values)
    })

    this.refresh()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  refresh() {
    this.dataSource.data = this.data;
    this.columnsToDisplay = [...this.getActivity(this.serviceId, this.activityId).attributes.map(attr => {
      return attr.name;
    })]
    this.displayedColumns = [...this.getActivity(this.serviceId, this.activityId).attributes.map(attr => {
      return attr.name;
    }), ...this.actionColumns]
    this.changeDetectorRefs.detectChanges();
  }


  onDeleteActivityClicked() {
    this.router.navigate(['/'], {state: {serviceIdForActivityToDelete: this.serviceId, activityIdToDelete: this.activityId}});
  }

  onEditActivityClicked() {
    const dialogRef = this.dialog.open(DialogEditActivity, {
      width: '600px',
      data: { firstValue: this.getActivity(this.serviceId, this.activityId).name, secondValue: this.getActivity(this.serviceId, this.activityId).description }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.getActivity(this.serviceId, this.activityId).name = result.firstValue;
        this.getActivity(this.serviceId, this.activityId).description = result.secondValue;
      }
    });
  }

  onAddAttributeClicked() {
    const dialogRef = this.dialog.open(DialogActivityAddAttribute, {
      width: '600px',
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result instanceof Attribute){
        this.getActivity(this.serviceId, this.activityId).addAttribute(result);
        this.refresh();
      }
    });
  }

  onAddExerciseClicked() {
    const dialogRef = this.dialog.open(DialogExerciseAdd, {
      width: '600px',
      data: {
        attributes: this.getActivity(this.serviceId, this.activityId).attributes
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && Array.isArray(result)) {
        let values: any[] = [];
        result.forEach((item) => {
          let name = item.name;
          let value = item.value;
          if (name !== undefined && value !== undefined) {
            if(value instanceof Date)
              values.push(new DateClass(value));
            else
              values.push(value);
          }
        })
        let tmpExercise = new Exercise(this.getActivity(this.serviceId, this.activityId).getAttributesNames(), values);
        this.getActivity(this.serviceId, this.activityId).addExercise(tmpExercise);
        this.data.push(tmpExercise.values);
        this.refresh();
      }
    });
  }

  onDeleteExercise(row: any) {
    const rowToDelete = row
    const indexToDelete = this.data.findIndex(f => f == row);
    const exerciseToDelete = this.getActivity(this.serviceId, this.activityId).exercises[indexToDelete];

    this.exercisesToBeDeleted.push(exerciseToDelete);
    this.indexOfExerciseToBeDeleted.push(indexToDelete);
    this.rowsToBeDeleted.push(rowToDelete);

    this.data = this.data.filter(f => f != row);
    this.getActivity(this.serviceId, this.activityId).deleteExercise(indexToDelete);
    this.refresh();

    if (!this.isTimeout || this.exercisesToBeDeleted.length != 1){
      this.isTimeout = false;
    }

    let snackbarText: string;
    if (this.exercisesToBeDeleted.length == 1) {
      snackbarText = "Výkon smazán";
    } else {
      snackbarText = "Výkony smazány (" + this.exercisesToBeDeleted.length + ")";
    }

    this.openedSnackBar = this._snackBar
      .open(snackbarText, 'Vrátit', {
        duration: 5000,
        horizontalPosition: "start",
        verticalPosition: "bottom"
      })

    this.openedSnackBar.onAction().subscribe(() => {
      this.isTimeout = true;
      for (let i = 0; i < this.exercisesToBeDeleted.length; i++) {
        this.getActivity(this.serviceId, this.activityId).addExerciseToIndex(this.indexOfExerciseToBeDeleted[i], this.exercisesToBeDeleted[i]);
        this.data.splice(this.indexOfExerciseToBeDeleted[i], 0, this.rowsToBeDeleted[i]);
      }
      this.refresh();
    })
    this.openedSnackBar.afterDismissed().subscribe( () => {
      if (this.isTimeout) {
        this.emptyDeletingArrays();
      }
      this.isTimeout = true;
    })
  }

  onModifyExercise(row: any) {
    const indexToModify = this.data.findIndex(f => f == row);
    const dialogRef = this.dialog.open(DialogExerciseEdit, {
      width: '600px',
      data: {
        exercise: this.getActivity(this.serviceId, this.activityId).exercises[indexToModify],
        attributes: this.getActivity(this.serviceId, this.activityId).attributes
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && Array.isArray(result)) {
        result.forEach((item) => {
          let name = item.name;
          let value = item.value;
          if (name !== undefined && value !== undefined) { 
            if(value instanceof Date)
            this.getActivity(this.serviceId, this.activityId).exercises[indexToModify].values[name] = new DateClass(value);
            else
            this.getActivity(this.serviceId, this.activityId).exercises[indexToModify].values[name] = value;
          }
        })
      }
    });
  }

  onExerciseDetailClicked(row: any) {
    const indexToShow = this.data.findIndex(f => f == row);
    const dialogRef = this.dialog.open(DialogExerciseDetail, {
      width: '600px',
      data: {
        exercise: this.getActivity(this.serviceId, this.activityId).exercises[indexToShow],
        attributes: this.getActivity(this.serviceId, this.activityId).attributes
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && Array.isArray(result)) {
        this.onModifyExercise(row);
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnDestroy() {
    if(this.openedSnackBar) {
      this.openedSnackBar.dismiss();
    }
  }

  // detects back button press
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    state.currentService = null;
  }

  emptyDeletingArrays() {
    this.exercisesToBeDeleted = [];
    this.indexOfExerciseToBeDeleted = [];
    this.rowsToBeDeleted = [];
  }

  getActivity(serviceId: number, activityId: number){
    return state.services[serviceId].activities[activityId];
  }
}
