import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Activity, Service} from "../../model";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {state} from "../../state";
import {DialogEditService} from 'src/app/dialogs/dialog-basic-create-edit/dialog-edit-service';
import {DialogAddActivity} from 'src/app/dialogs/dialog-basic-create-edit/dialog-add-activity';

@Component({
  selector: 'app-service-card',
  templateUrl: './service-card.component.html',
  styleUrls: ['./service-card.component.css']
})
export class ServiceCardComponent implements OnInit {
  @Input() service: Service;
  @Input() serviceId: number;
  @Output() deleteService = new EventEmitter();

  constructor(private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  onAddActivityClicked() {
    const dialogRef = this.dialog.open(DialogAddActivity, {
      width: '600px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined) {
        let name = result.firstValue;
        let desc = result.secondValue;
        this.service.activities.push(new Activity(name, desc));
      }
    });
  }

  onDeleteServiceClicked() {
    this.deleteService.emit()
  }

  onEditServiceClicked() {
    const dialogRef = this.dialog.open(DialogEditService, {
      width: '600px',
      data: {firstValue: this.service.name, secondValue: this.service.description}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined) {
        this.service.name = result.firstValue;
        this.service.description = result.secondValue;
      }
    });
  }

  onNavigateToActivityClicked(activity: Activity, activityId: number) {
    state.currentService = this.service;
    this.router.navigateByUrl("/service/" + this.serviceId + "/activity/" + activityId)
  }
}
