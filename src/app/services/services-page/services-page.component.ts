import {Component, OnDestroy, OnInit} from '@angular/core';
import { state } from "../../state";
import { Service } from "../../model";
import { MatDialog } from "@angular/material/dialog";
import { DialogAddService } from 'src/app/dialogs/dialog-basic-create-edit/dialog-add-service';
import {MatSnackBar, MatSnackBarRef, TextOnlySnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.component.html',
  styleUrls: ['./services-page.component.css']
})
export class ServicesPageComponent implements OnInit, OnDestroy{
  serviceIdForActivityToDelete: number;
  activityIdToDelete: number;
  private isTimeout:boolean = true;

  private servicesToBeDeleted: Service[] = [];
  private indexOfServicesToBeDeleted: number[] = [];

  private openedSnackBarDeleteService: MatSnackBarRef<TextOnlySnackBar>;
  private openedSnackBarDeleteActivityFromService: MatSnackBarRef<TextOnlySnackBar>;

  constructor(public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.deleteActivityFromService(history.state['serviceIdForActivityToDelete'], history.state['activityIdToDelete'])
  }

  addServiceClicked() {
    const dialogRef = this.dialog.open(DialogAddService, {
      width: '600px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined) {
        let name = result.firstValue;
        let desc = result.secondValue;
        state.services.push(new Service(name, desc, []));
      }
    });
  }

  deleteService(service: Service, index: number) {
    state.services = this.getServices().filter(it => it !== service);
    this.servicesToBeDeleted.push(service);
    this.indexOfServicesToBeDeleted.push(index);


    if (!this.isTimeout || this.servicesToBeDeleted.length != 1){
      this.isTimeout = false;
    }

    let snackbarText: string;
    if (this.servicesToBeDeleted.length == 1) {
      snackbarText = "Služba " + service.name + " smazána";
    } else {
      snackbarText = "Služby smazány (" + this.servicesToBeDeleted.length + ")";
    }

    let tempServices = this.servicesToBeDeleted;
    let tempIndexes = this.indexOfServicesToBeDeleted;

    this.openedSnackBarDeleteService = this._snackBar
      .open(snackbarText, 'Vrátit', {
        duration: 5000,
        horizontalPosition: "start",
        verticalPosition: "bottom"
      })

    this.openedSnackBarDeleteService.onAction().subscribe(() => {
      this.isTimeout = true;
      this.servicesToBeDeleted = tempServices;
      this.indexOfServicesToBeDeleted = tempIndexes;
      for (let i = 0; i < this.servicesToBeDeleted.length; i++) {
        this.getServices().splice(this.indexOfServicesToBeDeleted[i], 0, this.servicesToBeDeleted[i]);
      }
    })

    this.openedSnackBarDeleteService.afterDismissed().subscribe( () => {
      if (this.isTimeout) {
        this.emptyDeletingArrays();
      }
      this.isTimeout = true;    })
  }

  deleteActivityFromService(serviceId: number, activityId: number) {
    if(serviceId != undefined && activityId != undefined) {
      state.currentService = null;
      let service: Service = this.getServices()[serviceId];
      let activity = service.activities[activityId];
      service.activities.splice(activityId, 1);
      this.openedSnackBarDeleteActivityFromService = this._snackBar
        .open("Aktivita '" + activity.name + "' ze služby '" + service.name + "' smazána", 'Vrátit', {
        duration: 5000,
        horizontalPosition: "start",
        verticalPosition: "bottom"
      })

      this.openedSnackBarDeleteActivityFromService.onAction().subscribe(() => {
        service.activities.splice(activityId, 0, activity);
      })
    }
  }

  ngOnDestroy() {
    if(this.openedSnackBarDeleteService) {
      this.openedSnackBarDeleteService.dismiss();
    }
    if(this.openedSnackBarDeleteActivityFromService) {
      this.openedSnackBarDeleteActivityFromService.dismiss();
    }
  }

  getServices() :Service[] {
    return state.services;
  }

  emptyDeletingArrays() {
    this.servicesToBeDeleted = [];
    this.indexOfServicesToBeDeleted = [];
  }
}
