import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReportComponent } from './statistics/report/report.component';
import { MatChipsModule } from "@angular/material/chips";
import { MatPaginatorModule } from "@angular/material/paginator";
import { AppComponent } from './app.component';
import { DialogComponent } from './templates/dialog/dialog.component';
import { MenuComponent } from './menu/menu.component';
import { TestPageComponent } from './templates/test-page/test-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from "./app-routing.module";
import { ServiceCardComponent } from './services/service-card/service-card.component';
import { ServicesPageComponent } from './services/services-page/services-page.component';
import { StatisticsComponent } from "./statistics/statistics.component";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatNativeDateModule, MatOption, MAT_DATE_LOCALE } from "@angular/material/core";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTabsModule } from "@angular/material/tabs";
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input"
import { DialogDeleteExercise } from './dialogs/dialog-yes-no/dialog-delete-exercise';
import { FormsModule } from '@angular/forms';
import { DialogAddService } from './dialogs/dialog-basic-create-edit/dialog-add-service';
import { DialogDeleteActivity } from './dialogs/dialog-yes-no/dialog-delete-activity'
import { DialogEditService } from './dialogs/dialog-basic-create-edit/dialog-edit-service';
import { DialogAddActivity } from './dialogs/dialog-basic-create-edit/dialog-add-activity';
import { DialogEditActivity } from './dialogs/dialog-basic-create-edit/dialog-edit-activity';
import { ActivityComponent } from "./activity/activity.component";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatTableDataSource, MatTableModule } from "@angular/material/table";
import { DialogExerciseEdit } from './dialogs/dialog-exercise-edit-detail/dialog-exercise-edit';
import { DialogExerciseAdd } from './dialogs/dialog-exercise-edit-detail/dialog-exercise-add';
import { DialogExerciseDetail } from './dialogs/dialog-exercise-edit-detail/dialog-exercise-detail';
import {MatSort, MatSortModule} from "@angular/material/sort";
import {MatSnackBar, MatSnackBarModule} from "@angular/material/snack-bar";
import { DialogActivityAddAttribute } from './dialogs/dialog-activity-add-attribute/dialog-activity-add-attribute';
import { MatSelectModule } from '@angular/material/select';
import { EmptyPageComponent } from './templates/empty-page/empty-page.component';
import { WelcomePageComponent } from './templates/empty-page/welcome-page.component';
import { DialogCreateEnumDatatype } from './dialogs/dialog-create-enum-datatype/dialog-create-enum-datatype';

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
    MenuComponent,
    TestPageComponent,
    StatisticsComponent,
    ServiceCardComponent,
    ServicesPageComponent,
    DialogDeleteExercise,
    DialogDeleteActivity,
    DialogAddService,
    DialogEditService,
    DialogAddActivity,
    DialogEditActivity,
    ActivityComponent,
    DialogExerciseEdit,
    DialogExerciseAdd,
    DialogExerciseDetail,
    DialogActivityAddAttribute,
    EmptyPageComponent,
    WelcomePageComponent,
    ReportComponent,
    DialogCreateEnumDatatype
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    AppRoutingModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    MatTableModule,
    MatChipsModule,
    MatSortModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatSelectModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'cs-CZ'}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
