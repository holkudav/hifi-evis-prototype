import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Datatype } from 'src/app/model';
import { state } from "../../state";

@Component({
  selector: 'app-dialog-create-enum-datatype',
  templateUrl: './dialog-create-enum-datatype.component.html',
  styleUrls: ['./dialog-create-enum-datatype.component.css']
})
export class DialogCreateEnumDatatype implements OnInit {

  title = "Vytvořit datový typ";
  nameTitle = "Název";
  valuesTitle = "Přípustné hodnoty";
  newValue = "";
  datatype = new Datatype("", []);

  okButtonTitle = "Vytvořit";
  cancelButtonTitle = "Zrušit";
  constructor(public dialogRef: MatDialogRef<DialogCreateEnumDatatype>) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean {
    return this.datatype.name.length === 0 || this.datatype.allowedValues.length === 0;
  }

  onAddNewValueClick(): void {
    this.datatype.allowedValues.push(this.newValue);
    this.newValue = "";
  }

  onDatatypeValueClick(index: number): void {
    this.datatype.allowedValues.splice(index, 1);
  }

  onCreateClick(): void {
    state.datatypes.push(this.datatype);
  }
}
