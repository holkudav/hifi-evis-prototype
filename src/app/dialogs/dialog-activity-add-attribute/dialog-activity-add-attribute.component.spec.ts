import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActivityAddAttribute } from './dialog-activity-add-attribute';

describe('DialogActivityAddAttributeComponent', () => {
  let component: DialogActivityAddAttribute;
  let fixture: ComponentFixture<DialogActivityAddAttribute>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogActivityAddAttribute ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActivityAddAttribute);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
