import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Activity, Attribute, Datatype, Service } from 'src/app/model';
import { state } from "../../state";
import { DialogCreateEnumDatatype } from '../dialog-create-enum-datatype/dialog-create-enum-datatype';

@Component({
  selector: 'app-dialog-activity-add-attribute',
  templateUrl: './dialog-activity-add-attribute.component.html',
  styleUrls: ['./dialog-activity-add-attribute.component.css']
})
export class DialogActivityAddAttribute implements OnInit {

  title = "Přidat atribut";
  attributeTitle = "Název";
  attributeName = "";
  cancelButtonTitle = "Zrušit";
  okButtonTitle = "Přidat";

  serviceId: number;
  service: Service;

  activityId: number;
  activity: Activity;

  nullDatatype = new Datatype("", []);
  attribute: Attribute = new Attribute("", this.nullDatatype);

  datatypesTitle = "Datový typ";
  datatypes: Datatype[] = state.datatypes;

  constructor(public dialogRef: MatDialogRef<DialogActivityAddAttribute>, public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.serviceId = data.serviceId;
    this.activityId = data.activityId;
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean {
    return this.attribute.name.length === 0 || this.attribute.datatype === this.nullDatatype;
  }

  onCreateEnumDatatypeClick(): void {
    console.log(state.datatypes);
    const dialogRef = this.dialog.open(DialogCreateEnumDatatype, {
      width: '600px',
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result === true){
        console.log("truiuuu");
      }
      console.log(state.datatypes);
    });
  }
}


class DialogData {
  serviceId: number;
  activityId: number;
}
