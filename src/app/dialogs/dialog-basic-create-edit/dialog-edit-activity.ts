import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from './dialog-data';

@Component({
  selector: 'app-dialog-edit-activity',
  templateUrl: './dialog-basic-create-edit.component.html',
  styleUrls: ['./dialog-basic-create-edit.component.css']
})
export class DialogEditActivity implements OnInit {
  title = "Upravit aktivitu"
  firstName = "Název";
  secondName = "Popis";

  okButtonTitle = "Upravit";
  cancelButtonTitle = "Zrušit";

  constructor(
    public dialogRef: MatDialogRef<DialogEditActivity>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      if(data.firstValue === undefined) data.firstValue = "";
      if(data.secondValue === undefined) data.secondValue = "";
    }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean{
    return this.data.firstValue.length === 0 || this.data.secondValue.length === 0;
  }
}
