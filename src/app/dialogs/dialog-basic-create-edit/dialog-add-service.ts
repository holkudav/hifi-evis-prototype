import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from './dialog-data';

@Component({
  selector: 'app-dialog-add-service',
  templateUrl: './dialog-basic-create-edit.component.html',
  styleUrls: ['./dialog-basic-create-edit.component.css']
})
export class DialogAddService implements OnInit {
  title = "Vytvořit službu";
  firstName = "Název";
  secondName = "Popis";

  okButtonTitle = "Vytvořit";
  cancelButtonTitle = "Zrušit";

  constructor(
    public dialogRef: MatDialogRef<DialogAddService>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      if(data.firstValue === undefined) data.firstValue = "";
      if(data.secondValue === undefined) data.secondValue = "";
    }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean{
    return this.data.firstValue.length === 0 || this.data.secondValue.length === 0;
  }
}
