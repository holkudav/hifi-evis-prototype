import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from './dialog-data';

@Component({
  selector: 'app-dialog-edit-service',
  templateUrl: './dialog-basic-create-edit.component.html',
  styleUrls: ['./dialog-basic-create-edit.component.css']
})
export class DialogEditService implements OnInit{
  title = "Upravit službu"
  firstName = "Název";
  secondName = "Popis";

  okButtonTitle = "Upravit";
  cancelButtonTitle = "Zrušit";

  constructor(
    public dialogRef: MatDialogRef<DialogEditService>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      if(data.firstValue === undefined) data.firstValue = "";
      if(data.secondValue === undefined) data.secondValue = "";
    }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean{
    return this.data.firstValue.length === 0 || this.data.secondValue.length === 0;
  }
}
