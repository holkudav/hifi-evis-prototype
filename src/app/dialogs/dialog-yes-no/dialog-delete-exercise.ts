import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-yes-no',
  templateUrl: './dialog-yes-no.component.html',
  styleUrls: ['./dialog-yes-no.component.css']
})
export class DialogDeleteExercise implements OnInit {

  title = "Smazat výkon"
  text = "Opravdu chcete smazat tento výkon?"

  okButtonTitle = "Ano, smazat";
  cancelButtonTitle = "Ne, zrušit";
  constructor(public dialogRef: MatDialogRef<DialogDeleteExercise>) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
