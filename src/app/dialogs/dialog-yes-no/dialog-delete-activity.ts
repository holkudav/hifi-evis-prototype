import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-yes-no',
  templateUrl: './dialog-yes-no.component.html',
  styleUrls: ['./dialog-yes-no.component.css']
})
export class DialogDeleteActivity implements OnInit {

  title = "Smazat aktivitu"
  text = "Opravdu chcete smazat tuto aktivitu?"

  okButtonTitle = "Ano, smazat";
  cancelButtonTitle = "Ne, zrušit";
  constructor(public dialogRef: MatDialogRef<DialogDeleteActivity>) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
