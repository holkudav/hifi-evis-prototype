import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component, Inject, OnInit, ɵConsole } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Exercise, Attribute } from 'src/app/model';
import { DialogData, ValueWithName } from './dialog-exercise-add-edit-data';

@Component({
  selector: 'app-dialog-exercise-add-edit',
  templateUrl: './dialog-exercise-add-edit.component.html',
  styleUrls: ['./dialog-exercise-add-edit.component.css']
})
export class DialogExerciseDetail implements OnInit {

  title = "Detail výkonu"
  textReadonly = true;

  okButtonTitle = "Upravit";
  cancelButtonTitle = "Zavřít";

  valuesWithNames: ValueWithName[] = [];
  exercise: Exercise;
  attributes: Attribute[];

  constructor(public dialogRef: MatDialogRef<DialogExerciseDetail>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.exercise = this.data.exercise;
    this.attributes = this.data.attributes;
    for (let attr of this.attributes) {
      let val = new ValueWithName(attr.name, this.exercise.values[attr.name], attr.datatype);
      this.valuesWithNames.push(val);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean {
    return false;
  }
}