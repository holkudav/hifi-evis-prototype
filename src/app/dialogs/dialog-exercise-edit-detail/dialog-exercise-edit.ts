import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component, Inject, OnInit, ɵConsole } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Exercise, Attribute } from 'src/app/model';
import { DialogData, ValueWithName } from './dialog-exercise-add-edit-data';

@Component({
  selector: 'app-dialog-exercise-add-edit',
  templateUrl: './dialog-exercise-add-edit.component.html',
  styleUrls: ['./dialog-exercise-add-edit.component.css']
})
export class DialogExerciseEdit implements OnInit {

  title = "Upravit výkon"
  textReadonly = false;

  okButtonTitle = "Upravit";
  cancelButtonTitle = "Zrušit";

  valuesWithNames: ValueWithName[] = [];
  exercise: Exercise;
  attributes: Attribute[];

  constructor(public dialogRef: MatDialogRef<DialogExerciseEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.exercise = this.data.exercise;
    this.attributes = this.data.attributes;
    for (let attr of this.attributes) {
      let val = new ValueWithName(attr.name, this.exercise.values[attr.name], attr.datatype);
      this.valuesWithNames.push(val);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isEmpty(): boolean {
    let isEmpty = false;
    for (let value of this.valuesWithNames) {
      if (value.value === undefined || value.value.length === 0) {
        isEmpty = true;
        break;
      }
    }
    return isEmpty;
  }
}