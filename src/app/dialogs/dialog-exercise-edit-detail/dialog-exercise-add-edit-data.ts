import { Attribute, Datatype, Exercise } from 'src/app/model';

export class ValueWithName {
  constructor(public name: string, public value: string, public datatype: Datatype) { }
}

export interface DialogData {
  exercise: Exercise;
  attributes: Attribute[];
}