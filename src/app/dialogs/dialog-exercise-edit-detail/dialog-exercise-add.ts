import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Attribute } from 'src/app/model';
import { DialogData, ValueWithName } from './dialog-exercise-add-edit-data';

@Component({
  selector: 'app-dialog-exercise-add-edit',
  templateUrl: './dialog-exercise-add-edit.component.html',
  styleUrls: ['./dialog-exercise-add-edit.component.css']
})
export class DialogExerciseAdd implements OnInit {

  title = "Přidat výkon"
  textReadonly = false;

  okButtonTitle = "Přidat";
  cancelButtonTitle = "Zrušit";

  valuesWithNames: ValueWithName[] = [];
  attributes: Attribute[];

  constructor(public dialogRef: MatDialogRef<DialogExerciseAdd>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.attributes = this.data.attributes;
    for (let attr of this.attributes) {
      let val = new ValueWithName(attr.name, "", attr.datatype);
      this.valuesWithNames.push(val);
    }
  }

  onNoClick(): void {
    console.log(this.valuesWithNames);
    this.dialogRef.close();
  }

  isEmpty(): boolean {
    let isEmpty = false;
    for (let value of this.valuesWithNames) {
      if (value.value === undefined || value.value.length === 0) {
        isEmpty = true;
        break;
      }
    }
    return isEmpty;
  }
}

