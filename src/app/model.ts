import {map} from "rxjs/operators";
import {datatypes} from "./state";

export class Service {
  constructor(public name: string, public description: string, public activities: Activity[]) {
  }
}
export class Activity {

  private _exercises: Exercise[] = [];
  private _attributes: Attribute[] = [];
  constructor(public name: string, public description: string) {
    this
      .addAttribute(new Attribute("Zaměstnanec", datatypes[2]))
      .addAttribute(new Attribute("Klient", datatypes[2]))
      .addAttribute(new Attribute("Datum", datatypes[0]))
  }

  addExercise(exercise: Exercise) {
    this._exercises.push(exercise);
    return this;
  }

  addExerciseToIndex(startIndex: number, exercise: Exercise) {
    this._exercises.splice(startIndex, 0, exercise);
    return this;
  }

  deleteExercise(index: number) {
    this._exercises.splice(index,1)
  }

  get exercises(): Exercise[] {
    return this._exercises;
  }

  addAttribute(attribute: Attribute) {
    this._attributes.push(attribute);
    return this;
  }

  get attributes(): Attribute[] {
    return this._attributes;
  }

  getAttributesNames(): string[] {
    return this._attributes.map(attr => {
      return attr.name;
    })
  }

  addExerciseOnPos(exercise: Exercise, index: number) {
    this._exercises.splice(index, 0, exercise);
  }
}

export class Exercise {
  public values: PeriodicElement = {};
  constructor(attributeNames: string[], values: any[]) {
    for (let i = 0; i < attributeNames.length; i++) {
      this.values[attributeNames[i]] = values[i]
    }
  }
}

export class Attribute {
  constructor(public name: string, public datatype: Datatype ) {
  }

}

export class Datatype {
  constructor(public name: string, public allowedValues: string[]) {
  }

}

export interface PeriodicElement {
  [key: string]: any
}

export class DateClass extends Date{
  toString = this.toLocaleDateString;
}
