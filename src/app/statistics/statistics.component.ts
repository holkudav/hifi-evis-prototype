import { Component, OnInit } from '@angular/core';
import {Activity, Service} from "../model";
import {state} from "../state";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {ReportComponent} from "./report/report.component";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  step = 0;

  services: Service[] = state.services;
  emptyStatisticsTitle = "Žádné statistiky k dispozici";
  emptyStatisticsSubtitle = "Pro zobrazení statistik vytvořte aktivitu na stránce služeb";
  emptyStatisticsButtonText = "Služby";


  date = {
    start: null,
    end: null
  }

  selectedActivities: SelectedActivity[] = [];

  constructor(private router: Router,
              public dialog: MatDialog) {

  }

  ngOnInit(): void {
  }

  setStep(index: number) {
    this.step = index;
  }

  hasActivities(activities: Activity[]) : boolean {
    return !(activities == null || activities == [] || activities.length < 1);
  }

  isEmpty() : boolean {
    if (this.services == null) {
      return true;
    }
    for (let i = 0; i < this.services.length; ++i){
      if (this.hasActivities(this.services[i].activities)){
        return false;
      }
    }
    return true;
  }

  redirectToServices() : void {
    this.router.navigate(['']);
  }

  generateReport() : void {
    let activitiesChosen = [];

    for(let i = 0; i < this.selectedActivities.length; ++i){
      if(this.selectedActivities[i].selected){
        activitiesChosen.push(this.services[this.selectedActivities[i].serviceIndex].activities[this.selectedActivities[i].activityIndex].name);
      }
    }

    this.dialog.open(ReportComponent, {
      width: '80vw',
      data: {
        from: this.date.start,
        to: this.date.end,
        activities: activitiesChosen
      }
    });
  }

  changeActivitiesChosen(serviceIndexObtained: number, activityIndexObtained: number) : void {
    for (let i = 0; i < this.selectedActivities.length; ++i){
      if (serviceIndexObtained == this.selectedActivities[i].serviceIndex
      && activityIndexObtained == this.selectedActivities[i].activityIndex){
        this.selectedActivities[i].selected = !this.selectedActivities[i].selected;
        return;
      }
    }
    let newActivity = {
      serviceIndex: serviceIndexObtained,
      activityIndex: activityIndexObtained,
      selected: true
    }
    this.selectedActivities.push(newActivity);
  }

  activityIsSelected() :boolean{
    for (let i = 0; i < this.selectedActivities.length; ++i){
      if ( this.selectedActivities[i].selected) {
        return true;
      }
    }
    return false;
  }
}

export interface SelectedActivity {
  serviceIndex: number,
  activityIndex: number,
  selected: boolean
}
