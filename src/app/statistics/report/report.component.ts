import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ReportData} from "./reportData";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ReportComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReportData) {}

  ngOnInit(): void {
  }

  image="assets/chart.png";

  showDate (date: Date) : string {
    return date.getDate().toString() + '. ' + (date.getMonth() + 1).toString() + '. ' + date.getFullYear().toString()
  }

  closeReport() : void  {
    this.dialogRef.close();
  }
}
