export interface ReportData {
  from: Date;
  to: Date;
  activities: string[];
}
