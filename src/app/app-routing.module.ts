import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {StatisticsComponent} from "./statistics/statistics.component";
import {ServicesPageComponent} from "./services/services-page/services-page.component";
import {ActivityComponent} from "./activity/activity.component";
import {ReportComponent} from "./statistics/report/report.component";

const routes: Routes = [
  {path: "", component: ServicesPageComponent},
  {path: "service/:serviceId/activity/:activityId", component: ActivityComponent},
  {path: "report", component: ReportComponent},
  {path: "statistics", component: StatisticsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
