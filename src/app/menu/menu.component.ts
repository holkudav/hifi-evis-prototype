import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {state} from "../state";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  appState = state;

  links = [
    {label: 'Služby', link: ''},
    {label: 'Statistiky', link: '/statistics'},
  ];
  activeLink = this.links[0];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onLogoClicked() {
    this.activeLink = this.links[0];
    this.appState.currentService = null;
    this.router.navigate([this.activeLink.link]);
  }

}
