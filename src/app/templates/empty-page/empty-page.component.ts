import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-empty-page',
  templateUrl: './page-template.component.html',
  styleUrls: ['./page-template.component.css']
})
export class EmptyPageComponent implements OnInit {
  @Output() onButtonClick = new EventEmitter();

  @Input() title: string;
  @Input() subtitle: string;
  @Input() buttonText: string;
  image="assets/pixeltrue-seo.svg"

  constructor() { }

  ngOnInit(): void {
  }

}
