import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './page-template.component.html',
  styleUrls: ['./page-template.component.css']
})
export class WelcomePageComponent implements OnInit {
  @Output() onButtonClick = new EventEmitter();

  title="Vítejte v systému Evis";
  subtitle="Můžete začít vytvořením služby";
  buttonText="Vytvořit službu";
  image="assets/pixeltrue-welcome.svg"

  constructor() { }

  ngOnInit(): void {
  }

}
